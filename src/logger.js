import log from '@megatherium/log';

const logger = log.init({
	app: '@megatherium/rimraf-cli',
});

export default logger;
