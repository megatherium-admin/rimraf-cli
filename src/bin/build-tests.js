#!/usr/bin/env
/*global process,Promise*/
import {
	exists,
	mkdir,
	readdir,
	stat,
	writeFile,
} from '@megatherium/fs-promises';
import jsdoc from 'jsdoc-api';
import logger from './logger';
import packageJson from '../../package';
import path from 'path';

const log = logger.init('build-tests.js');

const getFilenames = async(dirname) => {
	const filenames = [],
		files = await readdir(dirname);
	await Promise.all(files.map(async(filename) => {
		filename = path.join(dirname, filename);
		const stats = await stat(filename);
		if (stats.isDirectory()) {
			const results = await getFilenames(filename);
			results.forEach((result) => {
				filenames.push(result);
			});
		} else {
			filenames.push(filename);
		}
	}));
	return filenames;
};

(async() => {
	const examples = [],
		filenames = await getFilenames(path.join(process.cwd(), 'src')),
		globals = [
			'afterAll',
			'beforeAll',
			'describe',
			'it',
			'require',
		];
	const results = (await jsdoc.explain({
		files: filenames,
	})).filter((result) => {
		return result.meta && result.examples && result.examples.length > 0;
	});

	results.forEach((result) => {
		if (result.meta.filename !== 'index.js' || results.length === 1) {
			result.examples.forEach((example) => {
				example = example.substring(example.search(/[\r\n]/) + 1).trim();

				// Parse imports
				const directImport = /import ([a-zA-Z0-9]+) from '([@]?[a-zA-Z0-9./]+(\/[a-zA-Z0-9-]+)?)';/gm,
					namedImport = /import ({[a-zA-Z0-9\s,]+}) from '([@]?[a-zA-Z0-9./]+(\/[a-zA-Z0-9-]+)?)';/gm;
				while (true) {
					let result = directImport.exec(example);
					if (result == null) {
						result = namedImport.exec(example);

						// For some reason, we need to execute this twice?
						if (result == null) {
							result = namedImport.exec(example);
						}
					}
					if (result == null) {
						break;
					}

					const [
						match,
						variableName,
					] = result;
					let moduleName = result[2];
					const isNamedImport = variableName.indexOf('{') >= 0;

					if (moduleName === packageJson.name) {
						moduleName = '../../..';
					}

					example = example.substring(0, result.index)
						+ `const ${ variableName } = require('${ moduleName }')${ !isNamedImport && (moduleName === '../../..' || moduleName.indexOf('@megatherium/') === 0) ? '.default' : '' };\n`
						+ example.substring(result.index + match.length);
				}

				// Remove self-executing async functions
				const selfExecutor = /\(async\(\) => \{(.*)\}\)\(\);/gms.exec(example);
				if (selfExecutor != null) {
					example = example.substring(0, selfExecutor.index)
						+ selfExecutor[1].replace(/\n\t/g, '\n')
						+ example.substring(selfExecutor.index + selfExecutor[0].length);
				}

				// Find global comments (https://eslint.org/docs/rules/no-undef).
				while (true) {
					const globalComments = /\/\/[\s]*global[\s]+([a-zA-Z,_]+)/g.exec(example);
					if (globalComments == null) {
						break;
					}

					globalComments[1].split(',').forEach((globalName) => {
						if (globals.indexOf(globalName) < 0) {
							globals.push(globalName);
						}
					});
					example = example.substring(0, globalComments.index)
						+ example.substring(globalComments.index + globalComments[0].length);
				}

				const name = result.longname.indexOf('module') === 0
					? result.longname.substring('module'.length + 1)
					: result.longname;
				examples.push({
					code: example,
					globals,
					meta: result.meta,
					name,
				});
			});
		}
	});

	const jsdocTestDirectory = path.join(process.cwd(), 'src', 'test', 'integration', 'jsdoc');
	if (!await exists(jsdocTestDirectory)) {
		await mkdir(jsdocTestDirectory);
	}

	if (examples.length > 0) {
		const testFilename = path.join(jsdocTestDirectory, 'index.js');
		await writeFile(testFilename, `/*global ${ globals.join(',') }*/
import setup from '../jsdocSetup';

afterAll(setup.afterAll);
beforeAll(setup.beforeAll);

${ examples.map((example) => {
		const code = example.code.trim().replace(/\n/g, '\n\t\t');
		if (code.length === 0) {
			return '';
		}
		return `describe('${ packageJson.name.substring('@megatherium/'.length) }.${ example.name }', () => {
	it('example should run without error (${ example.meta.filename }:${ example.meta.lineno })', async() => {
		${ code }
	});
});\n`;
	}).join('\n\n')}
`);
	}
})();
