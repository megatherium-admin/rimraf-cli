#!/usr/bin/env node
/*global process*/
import logger from './logger';
import path from 'path';
import rimraf from 'rimraf';

const log = logger.init('rimraf-cli.js');

const filename = path.join(process.cwd(), process.argv[2]);

rimraf.sync(filename);
log.info(`Deleted recursively ${ filename }.`);
